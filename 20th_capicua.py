"""
Escribir una función que devuelva el 20vo número capicúa
de 3 dígitos en el conjunto de los Números Naturales.
Imprimir el número encontrado como única salida del programa.
Así por ejemplo los números "101, 222, 373, 858, etc" son
números capicúas de 3 dígitos.
"""

import random

try:
    number = 100
    count = 1    
    while count <= 20:
        if (str(number) == str(number)[::-1]):
            count += 1
            if (count == 20): capicua_20th = number
        number += 1
    print(f'20th three-digit capicua number: {capicua_20th}')
except Exception as e:
    print(f'Oops! {e.__class__.__name__} occurred. {e}')