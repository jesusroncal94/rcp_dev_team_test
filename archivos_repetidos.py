"""
Nombre del script: archivos_repetidos.py

Escribir un programa que reciba el nombre de
una carpeta como parámetro y escanee todos los
archivos en dicha carpeta y subcarpetas
buscando archivos duplicados en términos del
contenido de los mismos, independientemente del
nombre de archivo y extensión.

Como salida deberá imprimir la primera
ocurrencia de cada archivo, su tamaño, su ruta
relativa, el número de archivos duplicados,
la suma total en bytes de los archivos
duplicados y el listado de rutas de los archivos
duplicados.
"""

import os, sys
import hashlib


def findDup(parentFolder):
    dups = {}
    for dirName, subdirs, fileList in os.walk(parentFolder):
        print(f'Scanning {dirName}...')
        for filename in fileList:
            path = os.path.join(dirName, filename)
            size = '... Each file weighs ' + str(os.path.getsize(path)) + ' bytes '
            file_hash = hashfile(path)
            if file_hash in dups:
                dups[file_hash].append(path)
                dups[file_hash].append(size)
            else:
                dups[file_hash] = [path]
    return dups

def joinDicts(dict1, dict2):
    for key in dict2.keys():
        if key in dict1:
            dict1[key] = dict1[key] + dict2[key]
        else:
            dict1[key] = dict2[key]

def hashfile(path, blocksize = 65536):
    afile = open(path, 'rb')
    hasher = hashlib.md5()
    buf = afile.read(blocksize)
    while len(buf) > 0:
        hasher.update(buf)
        buf = afile.read(blocksize)
    afile.close()
    return hasher.hexdigest()

def printResults(dict1):
    results = list(filter(lambda x: len(x) > 1, dict1.values()))
    if len(results) > 0:
        print('Duplicates Found:')
        print('The following files are identical. The name or extension could differ, but the content is identical')
        print('**********')
        for result in results:
            for subresult in result:
                print(f'\t{subresult}')
            print('**********')
    else:
        print('No duplicate files found.')


if len(sys.argv) > 1:
    dups = {}
    folders = sys.argv[1:]
    for i in folders:
        if os.path.exists(i):
            joinDicts(dups, findDup(i))
        else:
            print(f'{i} is not a valid path.')
            sys.exit()
    printResults(dups)
else:
    print('Usage: .../archivos_repetidos.py <folder path>')