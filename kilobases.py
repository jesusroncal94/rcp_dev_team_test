"""
!!! Esta pregunta no será calificada !!!

Nombre del script: kilobases.py

Escribir un programa que reciba en la línea de comandos como primer
parámetro obligatorio una cantidad de kilobases de ADN expresadas
como número un número flotante y como segundo parámetro opcional
un porcentaje de pares GC (Guanina­Citosina) expresado como un
número flotante entre 0 y 1 con un valor por omisión del 50%.

El programa deberá generar e imprimir en la salida estándar una
secuencia aleatoria de bases de la misma longitud teniendo en
cuenta que solo es posible combinar 4 valores distintos:
Guanina (G), Citosina (C), Adenina (A) y Timina (T) y que el
porcentaje exacto de pares GC debe ser respetado.

Así por ejemplo la secuencia TAGAGGTAGCCGTCACGATATCCGGAACTT está
compuesta por 30 bases (0.03 Kbases) con un 50% de pares GC ya
que hay exactamente 15 ocurrencias de las letras G ó C.
"""

