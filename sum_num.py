"""
Nombre del script: sum_num.py

Generar 5 números aleatorios enteros positivos,
calcular el mínimo y el máximo valor que se puede
obtener sumando los 4 máximos y los 4 mínimos
valores respectivamente.
"""

import random

try:
    max_number = int(input('Please specify the maximum number (greater than 0): '))
    if (max_number <= 0): raise Exception('Please the number must be greater than 0.')
    numbers = list(map(lambda n: random.randint(1, max_number), range(5)))
    high_numbers = sorted(numbers, reverse=True)[0:4]
    low_numbers = sorted(numbers, reverse=False)[0:4]

    print(f'List of 5 random numbers: {numbers}')
    print(f'List of the 4 highest numbers: {high_numbers}, sum: {sum(high_numbers)}')
    print(f'List of the 4 lowest numbers:: {low_numbers}, sum: {sum(low_numbers)}')
except Exception as e:
    print(f'Oops! {e.__class__.__name__} occurred. {e}')