"""
Nombre del script: repeated_elements.py

Buscar los elementos duplicados en un array. Ej:

Entrada: [4,3,2,7,8,2,3,1]
Salida: [2,3]

Considerar que el costo computacional no debe superar el O(n).
"""

import time

try:
    unique_elements, duplicate_elements = [], []
    list_elements = input('Enter a list elements separated by space: ').split()
    t0 = time.time()
    for n in list_elements:
        if n not in unique_elements: unique_elements.append(n)
        else:
            if n not in duplicate_elements: duplicate_elements.append(n)
    print(f'Input: {list_elements}')
    print(f'Output: {duplicate_elements}')
    print(f'Search algorithm runtime: {time.time() - t0}')
except Exception as e:
    print(f'Oops! {e.__class__.__name__} occurred. {e}')